import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteService } from '../services/route.service';
import { UserService } from '../services/user.service';
import { User } from '../model/user';
import {AppComponent} from '../app.component';
import { Route } from '../model/route';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ]
})
export class HomeComponent implements OnInit {
  recommended: Route[];
  topSport: Route[];
  topFamily: Route[];
  topCulture: Route[];
  topNight: Route[];
  topGastronomy: Route[];

  constructor(public userService: UserService, private routeService: RouteService, private router: Router,
              private appComponent: AppComponent) {
    this.appComponent.setMenuButton();
  }

  ngOnInit() {
    // On demande au backend
    this.routeService.getRecommended().subscribe(
      (routes) => {
        this.recommended = routes;
      }
    );

    this.routeService.getAllByType('sport').subscribe(
      (routes) => {
        this.topSport = routes;
      }
    );
    this.routeService.getAllByType('culture').subscribe(
      (routes) => {
        this.topCulture = routes;
      }
    );
    this.routeService.getAllByType('drink').subscribe(
      (routes) => {
        this.topNight = routes;
      }
    );
    this.routeService.getAllByType('gastronomic').subscribe(
      (routes) => {
        this.topGastronomy = routes;
      }
    );
  }

  onRouteClick(route) {
    this.routeService.currentRouteDetails = route;
    this.router.navigateByUrl('route-details');
  }
}

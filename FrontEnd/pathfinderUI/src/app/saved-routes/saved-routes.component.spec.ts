import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedRoutesComponent } from './saved-routes.component';

describe('SavedRoutesComponent', () => {
  let component: SavedRoutesComponent;
  let fixture: ComponentFixture<SavedRoutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedRoutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedRoutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

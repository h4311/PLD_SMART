import { Component, OnInit } from '@angular/core';
import {RouteService} from '../services/route.service';
import {Router} from '@angular/router';
import {Route} from '../model/route';

@Component({
  selector: 'app-saved-routes',
  templateUrl: './saved-routes.component.html',
  styleUrls: ['./saved-routes.component.css']
})
export class SavedRoutesComponent implements OnInit {

  savedRoutes: Route[];

  constructor(private router: Router, private routeService: RouteService) {
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    this.routeService.getSavedRoute().subscribe(
      (routes) => {
        this.sortRoutesList(routes);
        this.savedRoutes = routes;
      },
      error => console.error(error)
    );
  }
  ngOnInit() {
  }
  sortRoutesList(list) {
    list.sort(function(a, b) {
      if (a['dateTime'] < b['dateTime']) {
        return 1;
      } else if (a['dateTime'] > b['dateTime']) {
        return -1;
      }
      return 0;
    });
  }
  onRouteClick(route: Route) {
    this.routeService.currentRouteDetails = route;
    this.router.navigateByUrl('route-details');
  }
  deleteRoute(route) {
    this.routeService.deleteRoute(route.id).subscribe(
      (data) => {
      this.savedRoutes.filter(function(el) {
          return el.id !== route.id;
        });
      });
  }
}

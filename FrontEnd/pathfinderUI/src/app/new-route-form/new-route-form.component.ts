import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RouteService } from '../services/route.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-new-route-form',
  templateUrl: './new-route-form.component.html',
  styleUrls: [ './new-route-form.component.css' ]
})
export class NewRouteFormComponent implements OnInit {
  trackRequest = { category: '', origin: '', destination: '', passingPoints: [] };
  form = new FormGroup({
    category: new FormControl('', [ Validators.required ]),
    origin: new FormControl('', []),
    destination: new FormControl('', []),
    passingPoints: new FormControl('', ),
  });
  origin;
  destination;

  originName = 'Unknown place';
  destinationName = 'Unknown place';
  createButtonDisabled = true;

  constructor(private routeService: RouteService, private router: Router, private route: ActivatedRoute,
              private appComponent: AppComponent) {
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    this.appComponent.setMenuButton();
    this.route.queryParams.subscribe(params => {
      if (params.startLat && params.startLng && params.endLat && params.endLng) {
        this.origin = {latitude: params.startLat , longitude: params.startLng};
        this.destination = {latitude: params.endLat , longitude: params.endLng};

        this.trackRequest.origin = params.startLat + ',' + params.startLng;
        this.trackRequest.destination = params.endLat + ',' + params.endLng;
      }
      if (params.category) {
        this.trackRequest.category = params.category;
        console.log(this.trackRequest.category);
      }
      this.checkButtonDisabled();
    });
  }

  placeMarker($event) {
    if (this.origin) {
      if (!this.destination) {
        this.destination = {latitude: $event.coords.lat, longitude: $event.coords.lng};
      }
    } else {
      this.origin = {latitude: $event.coords.lat, longitude: $event.coords.lng};
    }
    this.checkButtonDisabled();
  }

  checkButtonDisabled() {
    if (this.origin && this.destination && this.trackRequest.category) {
      this.createButtonDisabled = false;
    }
  }

  ngOnInit() {
  }

  getRoute() {
    Observable.combineLatest(
      this.routeService.getNameLocationGoogle(this.origin),
      this.routeService.getNameLocationGoogle(this.destination)
    ).subscribe(dataGoogle => {
      console.log(dataGoogle[0].results[0].address_components);
      for (const elem of dataGoogle[0].results[0].address_components) {
        console.log(elem.types);
        if (elem.types.includes( 'route')) {
          this.originName = elem.long_name;
          break;
        }
      }
      for (const elem of dataGoogle[1].results[0].address_components) {
        if (elem.types.includes( 'route')) {
          this.destinationName = elem.long_name;
          break;
        }
      }
      console.log(this.originName + ' -> ' + this.destinationName);
      // On demande au backend
      this.routeService.getRoute(this.trackRequest.category, this.origin,
        this.destination, this.originName, this.destinationName).subscribe(
        (route) => {
          console.log(route);
          this.routeService.storeTempRoute(route);
          this.router.navigateByUrl('/route-view');
        }/*,
          err => {
            this.routeService.currentRoute = {
              origin: this.origin,
              destination: this.destination,
              pointsOfInterest: [
                {name: 'test1', position: {latitude: 45.777917, longitude: 4.865}},
                {name: 'test2', position: {latitude: 45.777917, longitude: 4.870}}]
            };
            this.router.navigateByUrl('/route-view');
          }*/);
    });

  }

  searchStartPoint() {
    this.routeService.getCoordinatesGoogle(this.trackRequest.origin + ',lyon')
    .subscribe(data => {
      this.origin = {latitude: data.results[ 0 ].geometry.location.lat, longitude: data.results[ 0 ].geometry.location.lng};
      console.log(this.trackRequest.origin + ' ' + this.origin.latitude + ' ' + this.origin.longitude);
    });
    this.checkButtonDisabled();
  }

  searchEndPoint() {
    this.routeService.getCoordinatesGoogle(this.trackRequest.destination + ',lyon')
      .subscribe(data => {
        this.destination = {latitude: data.results[ 0 ].geometry.location.lat, longitude: data.results[ 0 ].geometry.location.lng};
        console.log(this.trackRequest.destination + ' ' + this.destination.latitude + ' ' + this.destination.longitude);
      });
    this.checkButtonDisabled();
  }

  markerMoved(isOrigin, $event) {
    if (isOrigin) {
      this.origin = {latitude: $event.coords.lat, longitude: $event.coords.lng};
    } else {
      this.destination = {latitude: $event.coords.lat, longitude: $event.coords.lng};
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import {RouteService} from './services/route.service';

@Injectable()
export class CurrentRouteGuard implements CanActivate {

  constructor(private routeService: RouteService) {}

  canActivate() {
    console.log(this.routeService.currentRoute);
    return this.routeService.currentRoute !== undefined;
  }
}

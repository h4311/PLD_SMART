import { Component, OnInit } from '@angular/core';

import { User } from '../model/user';
import { UserService } from '../services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {AppComponent} from '../app.component';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: [ './sign-up.component.css' ],
})
export class SignUpComponent implements OnInit {

  user: User = new User();
  passwordC: string;

  form = new FormGroup({
    name: new FormControl('', [ Validators.required ]),
    emailControl: new FormControl('', [ Validators.required, Validators.email ]),
    password: new FormControl('', Validators.minLength(5)),
    passwordConfirm: new FormControl('', Validators.minLength(5)),
  }, SignUpComponent.passwordMatchValidator);

  constructor(private userService: UserService, private router: Router, private snackBar: MatSnackBar,
              private appComponent: AppComponent) {
    if (localStorage.getItem('connectedUser')) {
      this.router.navigateByUrl('/');
      return;
    }
    appComponent.setMenuButton();
  }

  static passwordMatchValidator(g: FormGroup) {
    return g.get('password').value === g.get('passwordConfirm').value
      ? null : { 'mismatch': true };
  }

  getErrorMessageEmail() {
    return this.form.get('emailControl').hasError('required') ? 'You must enter a value' :
      this.form.get('emailControl').hasError('emailControl') ? 'Not a valid emailControl' :
        '';
  }

  getErrorMessagePassword() {
    return this.form.get('password').hasError('minLength') ? 'Password must be 5 characters min' : '';
  }

  ngOnInit() {
    if (localStorage.getItem('connectedUser')) {
      this.router.navigateByUrl('/home');
    }
  }

  get diagnostic() {
    return JSON.stringify(this.user);
  }

  onSubmit() {
    this.newUser(this.user);
  }

  newUser(user: User): void {
    this.userService.postNewUser(user).subscribe((response: User) => {
      console.log(response);
      this.openSnackBar('The user was created', 1000);
      this.userService.loginUser(user.name, user.password)
        .subscribe(
          (result: string) => {
            console.log(result);
            console.log('connection success');
            this.userService.getUser().subscribe(
              next => {
              console.log('get user success');
              this.router.navigate(['preferences']);
              this.openSnackBar('You are connected', 1000);
            },
              error => console.error('get user failed')
            );
          });
    }, error1 => {
      this.openSnackBar('The user could not be created. Please change the username', 8000);
    });
  }


  openSnackBar(message: string, time: number) {
    this.snackBar.open(message, null, {
      duration: time,
    });
  }
}

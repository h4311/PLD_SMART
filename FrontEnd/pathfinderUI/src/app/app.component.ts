import { ChangeDetectorRef, Component } from '@angular/core';
import { UserService } from './services/user.service';
import { RouteService } from './services/route.service';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Location} from '@angular/common';

/*import {MediaMatcher} from '@angular/cdk/layout';*/

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [ 'app.component.css' ],
})


export class AppComponent {
  title = 'PathFinder';
  mat_toolbar_button = 'menu';
  // mobileQuery: MediaQueryList;
  // private _mobileQueryListener: () => void;
  //
  // constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
  //   this.mobileQuery = media.matchMedia('(max-width: 600px)');
  //   this._mobileQueryListener = () => changeDetectorRef.detectChanges();
  //   this.mobileQuery.addListener(this._mobileQueryListener);
  // }
  //
  // ngOnDestroy(): void {
  //   this.mobileQuery.removeListener(this._mobileQueryListener);
  // }
  constructor(public userService: UserService, private routeService: RouteService,
              private router: Router, private snackBar: MatSnackBar, private location: Location) {
    this.userService.connectedUser = JSON.parse(localStorage.getItem('connectedUser'));
  }

  disconnect() {
    if (this.userService.disconnectUser()) {
      this.openSnackBar('You have been disconnected', 1000);
      console.log('disconnection success !');
      this.routeService.disconnect();
    } else {
      this.openSnackBar('We could not disconnect you. It\'s probably an alien interference', 8000);
      console.error('disconnection failed');
    }
    this.router.navigate([ 'home' ]);
  }

  get connected(): boolean {
    if (localStorage.getItem('connectedUser')) {
      return true;
    }
    return false;
  }

  get routeExist(): boolean {
    if (localStorage.getItem('currentRoute')) {
      return true;
    }
    return false;
  }

  openSnackBar(message: string, time: number) {
    this.snackBar.open(message, null, {
      duration: time,
    });
  }

  back() {
    this.location.back();
  }

  setBackButton() {
      this.mat_toolbar_button = 'arrow_back';
  }

  setMenuButton() {
      this.mat_toolbar_button = 'menu';
  }
}

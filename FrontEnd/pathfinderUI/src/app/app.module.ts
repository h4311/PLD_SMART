import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';


import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { SignUpComponent } from './sign-up/sign-up.component';

import { RouteService } from './services/route.service';
import { UserService } from './services/user.service';
import { LogInComponent } from './log-in/log-in.component';
import { NewRouteFormComponent } from './new-route-form/new-route-form.component';
import { RouteViewComponent } from './route-view/route-view.component';
import { HistoryComponent } from './history/history.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';

import { DirectionsMapDirective } from './route-view/googleapidirection.directive';
import { HomeComponent } from './home/home.component';
import {CurrentRouteGuard} from './app-routing.module';
import { EndRouteConfirmationComponent } from './route-view/end-route-confirmation/end-route-confirmation.component';
import { CancelRouteConfirmationComponent } from './route-view/cancel-route-confirmation/cancel-route-confirmation.component';
import { RateRouteComponent } from './route-view/rate-route/rate-route.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { RouteDetailsComponent } from './route-details/route-details.component';
import { SavedRoutesComponent } from './saved-routes/saved-routes.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'route-details', component: RouteDetailsComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'log-in', component: LogInComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'saved-routes', component: SavedRoutesComponent },
  { path: 'new-route-form', component: NewRouteFormComponent },
  { path: 'route-view', component: RouteViewComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'preferences', component: PreferencesComponent}
];

@NgModule({
  imports: [
    HttpClientModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCLVUJdFHS5CxCP3DVRJAIdzF4U8O3HTxs'
    }),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,

  ],
  providers: [RouteService, UserService],
  declarations: [ AppComponent, SignUpComponent, LogInComponent, NewRouteFormComponent, RouteViewComponent,
    HistoryComponent, AboutUsComponent, ContactComponent, DirectionsMapDirective,
    HomeComponent, EndRouteConfirmationComponent, CancelRouteConfirmationComponent, RateRouteComponent, PreferencesComponent, RouteDetailsComponent, SavedRoutesComponent ],
  bootstrap: [ AppComponent ],
  entryComponents: [EndRouteConfirmationComponent, CancelRouteConfirmationComponent, RateRouteComponent]
})
export class AppModule {
}

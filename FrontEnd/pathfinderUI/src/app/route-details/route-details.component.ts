import { Component, OnInit } from '@angular/core';
import { Route} from '../model/route';
import {Review} from '../model/review';
import {ActivatedRoute, Router} from '@angular/router';
import {RouteService} from '../services/route.service'
import {Coordinates} from '../model/coordinates';

@Component({
  selector: 'app-route-details',
  templateUrl: './route-details.component.html',
  styleUrls: ['./route-details.component.css']
})
export class RouteDetailsComponent implements OnInit {

  route: Route;
  waypointsCoordinates: Coordinates[];
  routeIsBeginned: boolean;


  constructor(private router: Router, private routeService: RouteService) {
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    this.route = this.routeService.currentRouteDetails;
    console.log(this.route)
    if ((localStorage.getItem('currentRoute'))) {
      this.routeIsBeginned=true;
    }
    this.route.waypoints.forEach(function (element) {
      element['iconUrl'] = 'assets/marker-yellow.png';
    });
    this.route.origin['iconUrl'] = 'assets/marker-green.png';
    this.route.destination['iconUrl'] = 'assets/marker-red.png';
    this.waypointsCoordinates = this.route.waypoints.map(poi => poi.coordinates);
  }

  ngOnInit() {
  }

  buttonHome() {
    this.router.navigateByUrl('');
  }

  buttonGo() {
    //this.routeService.currentRoute = this.route;
    //this.router.navigateByUrl('/route-view');
    this.routeService.storeTempRoute(this.route);
    this.routeIsBeginned = true;
    this.routeService.storeCurrentRoute(this.route);
    this.routeService.startRoute().subscribe((route:Route)=>{
      this.router.navigateByUrl('/route-view');
    })
  }

  poiSelected = null;
  infoWindow = null;

  clickedMarker(pos, infoWindow) {
    this.markerToggle(pos);
    if (this.poiSelected != null) {
      this.markerToggle(this.poiSelected);
    }
    this.poiSelected = pos;

    if ( this.infoWindow ===  infoWindow) {
      return;
    }
    if (this.infoWindow !== null) {
      this.infoWindow.close();
    }
    this.infoWindow = infoWindow;
  }

  selectionnedMarkerColor = 'assets/marker-blue.png';

  markerToggle(pos) {
    let baseUrl;
    if (pos === this.route.origin) {
      baseUrl = 'assets/marker-red.png';
      } else if (pos === this.route.destination) {
      baseUrl = 'assets/marker-green.png';
    } else {
      baseUrl = 'assets/marker-yellow.png';
    }
    if (pos.iconUrl === baseUrl) {
      pos.iconUrl = this.selectionnedMarkerColor;
    } else {
      pos.iconUrl = baseUrl;
    }
  }

  getColor(nb, note) {
    return nb > note ? 'disabled' : 'warn';
  }

}

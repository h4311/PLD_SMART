import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../model/user';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {
  private userURL = 'http://localhost:8080/user';
  private headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    withCredentials: true
  };

  connectedUser: User = undefined;

  // connectedUser: User = {name: 'first', email: 'm@m.m', password: 'pass'};

  postNewUser(user: User) {
    const params = {
      name: user.name,
      email: user.email,
      password: user.password
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    return this.http.post(this.userURL + '/add', {}, { params, headers });
  }

  constructor(private http: HttpClient) {
  }

  loginUser(name: string, password: string) {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      responseType: 'text' as 'text'
    };
    const param = new HttpParams().set('username', name).set('password', password);
    return this.http.post('http://localhost:8080/login', param, headers);
  }

  getUser() {
    return this.http.get<any>(this.userURL + '/getLoggedUser', this.headers)
      .map(user => this.updateUser(user));
  }

  updateUser(user: User) {
    this.connectedUser = user;
    localStorage.setItem('connectedUser', JSON.stringify(user));
  }

  disconnectUser() {
    return this.http.post('http://localhost:8080/logout', {}, { withCredentials: true })
      .subscribe(
      next => {
        this.connectedUser = undefined;
        localStorage.removeItem('connectedUser');
        return true;
      },
      error => false
    );
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  }

  setPreferences(preferences) {
    const options = { ...this.headers, params: preferences };

    return this.http.post(this.userURL + '/configurePreferences', {}, options);
  }
}

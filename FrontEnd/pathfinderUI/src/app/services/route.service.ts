import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Route } from '../model/route';
import { Coordinates } from '../model/coordinates';
import {PointOfInterest} from "../model/point-of-interest";
import {of} from 'rxjs/observable/of';
import {track} from '../model/moke-poi';

interface Point {
  id: number;
  grandLyonId: string;
  googleMapsId: string;
  name: string;
  coordinates: {
    latitude: number;
    longitude: number;
  };
  address: string;
  rating: number;
  mainType: string;
  types: string[];
}

@Injectable()
export class RouteService {
  private PoiURL = 'http://localhost:8080/pointofinterest';
  private RouteURL = 'http://localhost:8080/route';
  private headers = {
    headers: new HttpHeaders({
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
    }),
    withCredentials: true
  };

  currentRoute: Route = undefined;
  tempRoute: Route = undefined;
  currentRouteDetails: Route = undefined;

  getRoute(category, origin, destination, originName, destinationName) {
    const param = JSON.stringify({
      type: category, origin: origin, destination: destination,
      originName: originName, destinationName: destinationName
    });
    console.log(param);
    return this.http.post<Route>(this.RouteURL + '/generateRouteOriginDestination', param, this.headers
    );
  }

  getCoordinatesGoogle(term: string): any {
    return this.http.get('https://maps.google.com/maps/api/geocode/json?address=' + term + '&key=AIzaSyCLVUJdFHS5CxCP3DVRJAIdzF4U8O3HTxs');
  }

  getNameLocationGoogle(coordinates: Coordinates): any {
    const latlng = coordinates.latitude + ',' + coordinates.longitude;
    return this.http.get('https://maps.google.com/maps/api/geocode/json?latlng=' + latlng + '&key=AIzaSyCLVUJdFHS5CxCP3DVRJAIdzF4U8O3HTxs');
  }

  constructor(private http: HttpClient) {
  }

  storeTempRoute(route: Route) {
    this.tempRoute = route;
    localStorage.setItem('tempRoute', JSON.stringify(route));
  }

  storeCurrentRoute(route: Route) {
    this.currentRoute = route;
    localStorage.setItem('currentRoute', JSON.stringify(route));
  }

  resetCurrentRoute() {
    this.currentRoute = undefined;
    localStorage.removeItem('currentRoute');
  }

  resetTempRoute() {
    this.tempRoute = undefined;
    localStorage.removeItem('tempRoute');
  }

  startRoute() {
    const param = new HttpParams().set('id', this.tempRoute.id.toString());
    return this.http.get(this.RouteURL + '/startRoute', { params: param, withCredentials: true });
  }

  saveRoute() {
    const param = new HttpParams().set('id', this.tempRoute.id.toString());
    return this.http.get(this.RouteURL + '/saveRoute', { params: param, withCredentials: true });
  }

  deleteRoute(id) {
    const param = new HttpParams().set('id', id.toString());
    return this.http.get(this.RouteURL + '/deleteSavedRoute', { params: param, withCredentials: true });
  }

  endRoute() {
    return this.http.get(this.RouteURL + '/endRoute', this.headers);
  }

  cancelRoute() {
    return this.http.get(this.RouteURL + '/cancelRoute', this.headers);
  }

  addReview(routeId, note, comment) {
    const param = JSON.stringify({ id: routeId, note: note, comment: comment });
    return this.http.post<Route>(this.RouteURL + '/addReview', param, this.headers
    );
  }

  addPoiReview(poiId, note, comment) {
    console.log('trying to post poi review')
    const param = JSON.stringify({ id: poiId, note: note, comment: comment });
    return this.http.post(this.PoiURL + '/addReview', param, this.headers);
  }

  getCurrentRoute() {
    return this.http.get<Route>(this.RouteURL + '/getCurrentRoute', this.headers);
  }

  getRecommended() {
    return this.http.get<Array<Route>>(this.RouteURL + '/getRecommended', this.headers);
  }

  getAllByType(type) {
    const param = new HttpParams().set('type', type );
    return this.http.get<Array<Route>>(this.RouteURL + '/getAllByType', { params: param, withCredentials: true });
  }

  getHistory() {
    return this.http.get<Array<Route>>(this.RouteURL + '/getHistory', this.headers);
  }

  getSavedRoute() {
    return this.http.get<Array<Route>>(this.RouteURL + '/getSaved', this.headers);
  }

  disconnect() {
    this.currentRoute = undefined;
    localStorage.removeItem('currentRoute');
  }
}


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouteService } from '../services/route.service';
import { Route } from '../model/route';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  history: Route[];

  constructor(private router: Router, private routeService: RouteService) {
    console.log('history is constructed');
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    this.routeService.getHistory().subscribe(
      (routes) => {
        this.sortRoutesList(routes);
        console.log(routes);
        this.history = routes;
      },
      error => console.error(error)
    );
  }
  ngOnInit() {
    console.log('history is initialized');
  }
  sortRoutesList(list) {
    list.sort(function(a, b) {
      if (a['dateTime'] < b['dateTime']) {
        return -1;
      } else if (a['dateTime'] > b['dateTime']) {
        return 1;
      }
      return 0;
    });
  }
  onRouteClick(route: Route) {
    this.routeService.currentRouteDetails = route;
    this.router.navigateByUrl('route-details');
  }
}

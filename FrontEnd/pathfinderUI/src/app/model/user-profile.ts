import {UserPreferences} from './user-preferences';

export class UserProfile {
  constructor(
    public notes: UserPreferences,
    public id: number = null
  ) {
  }
}

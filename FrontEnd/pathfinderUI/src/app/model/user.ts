import {Route} from './route';
import {UserProfile} from './user-profile';

export class User {
  constructor(
    public name: string = null,
    public email: string = null,
    public password: string = null,
    public role: string = 'user',
    public userProfile: UserProfile = null,
    public routes: Array<Route> = [],
    public currentRoute: Route = null,
    public id: number = null
  ) {
  }
}

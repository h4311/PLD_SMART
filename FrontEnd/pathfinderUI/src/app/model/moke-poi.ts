
import { PointOfInterest } from './point-of-interest';
import {Route} from './route';
import {Review} from './review';
import {USER} from './moke-user';

const reviews: Review[] = [{user: USER, comment: 'Nice', note: 4}, {user: USER, comment: 'Bof', note: 2}];
const waypoints: PointOfInterest[] = [
  {grandLyonId: '1', googleMapsId: '2', name: 'test1', coordinates: {latitude: 45.777917, longitude: 4.865}, address: '1 rue machin',
    rating: 2, mainType: 'Sport', reviews: reviews, types: ['Sport']}];

export const  track: Route = {
  id: 1,
  creator: USER,
  reviews: reviews,
  rating: 2,
  reviewsNr: 2,
  origin: {latitude:   45.777917, longitude:   4.86},
  destination: {latitude:   45.777917, longitude:   4.875},
  originName: 'bellecoour',
  destinationName: 'villeurbanne',
  waypoints: waypoints,
  radius: 100,

   dateTime: '2017',
   type: 'culture'
};


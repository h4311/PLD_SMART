import { User } from './user';
import {UserProfile} from './user-profile';
import {UserPreferences} from './user-preferences';

const userPreferences: UserPreferences = {
  sport: 3, drink: 3, gastronomic: 3, culture: 3
};
const userProfile: UserProfile = {
  notes: userPreferences, id: 2
};
export const USER: User = { name: 'Pierre', email: 'm@m.m', password: 'pass', role: 'user',
  userProfile: userProfile, routes: [], currentRoute: null, id: 2};

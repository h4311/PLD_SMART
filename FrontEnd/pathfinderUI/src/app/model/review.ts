import {User} from './user';

export class Review {
  constructor(
    public user: User,
    public comment: string,
    public note: number
  ) {  }
}


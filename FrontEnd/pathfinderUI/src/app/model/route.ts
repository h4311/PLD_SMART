import { PointOfInterest } from './point-of-interest';
import { Coordinates } from './coordinates';
import { User } from './user';
import { Review } from './review';


export class Route {
  constructor(
    public id: number,
    public creator: User,
    public reviews: Review[],
    public rating: number,
    public reviewsNr: number,
    public origin: Coordinates,
    public destination: Coordinates,
    public originName: string,
    public destinationName: string,
    public waypoints: PointOfInterest[],
    public radius: number,
    public dateTime: string,
    public type: string,
  ) {
  }
}

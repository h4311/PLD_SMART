import {Coordinates} from './coordinates';
import {Review} from './review';

export class PointOfInterest {
  constructor(
    public grandLyonId: string,
    public googleMapsId: string,
    public name: string,
    public coordinates: Coordinates,
    public address: string,
    public rating: number,
    public mainType: string,
    public reviews: Review[],
    public types: string[]
  ) {}
}

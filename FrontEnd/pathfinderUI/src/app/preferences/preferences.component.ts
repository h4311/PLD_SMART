import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../model/user';
import {MatSnackBar} from '@angular/material';
import {AppComponent} from '../app.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.component.html',
  styleUrls: ['./preferences.component.css'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: false
})

export class PreferencesComponent implements OnInit {

  constructor(private userService: UserService, private router: Router, private snackBar: MatSnackBar, private appComponent: AppComponent) {
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    appComponent.setBackButton();
    if (userService.connectedUser) {
      this.updatePreferences(userService.connectedUser);
    }
  }

  ngOnInit() {
  }

  autoTicks = false;
  disabled = false;
  invert = false;
  max = 5;
  min = 0;
  showTicks = true;
  step = 1;
  thumbLabel = true;
  vertical = false;
  preferences = {
    sport: {
      title: 'Sport',
      value: 3
    },
/*    family: {
      title: 'Family',
      value: 3
    },*/
    culture: {
      title: 'Culture',
      value: 3
    },
    drink: {
      title: 'Drinks',
      value: 3
    },
    gastronomic: {
      title: 'Gastronomy',
      value: 3
    },
/*    solo: {
      title: 'Solo',
      value: 3
    }*/
  };

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }

  private _tickInterval = 1;

  onSubmit() {
    const preferences = {};

    Object.keys(this.preferences).map(key => preferences[key] = this.preferences[key].value);

    this.userService.setPreferences(preferences).subscribe((user: User) => {
      this.userService.updateUser(user);
      this.updatePreferences(user);
      this.openSnackBar('Your preferences are updated', 1000);
    }, error1 => {
      this.openSnackBar('We couldn\'t save your preferences. Please contact us to report the bug', 3000);
    });
  }

  getPreferencesList() {
    return Object.keys(this.preferences).map(key => this.preferences[key]);
  }

  updatePreferences(user: User) {
    if (!user.userProfile || !user.userProfile.notes) {
      return;
    }

    const preferences = user.userProfile.notes;

    Object.keys(preferences).map(key => {
      if (this.preferences[key]) {
        this.preferences[key].value = preferences[key];
      }
    });
  }
  openSnackBar(message: string, time: number) {
    this.snackBar.open(message, null, {
      duration: time,
    });
  }
}

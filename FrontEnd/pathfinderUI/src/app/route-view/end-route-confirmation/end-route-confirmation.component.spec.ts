import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndRouteConfirmationComponent } from './end-route-confirmation.component';

describe('EndRouteConfirmationComponent', () => {
  let component: EndRouteConfirmationComponent;
  let fixture: ComponentFixture<EndRouteConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndRouteConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndRouteConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

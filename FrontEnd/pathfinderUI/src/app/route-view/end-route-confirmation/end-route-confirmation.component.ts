import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-end-route-confirmation',
  templateUrl: './end-route-confirmation.component.html',
  styleUrls: ['./end-route-confirmation.component.css']
})
export class EndRouteConfirmationComponent implements OnInit {

  constructor(    private dialogRef: MatDialogRef<EndRouteConfirmationComponent>) { }

  ngOnInit() {
  }

  endRoute() {
    this.dialogRef.close(true);
  }

  goBack() {
    this.dialogRef.close(false);
  }
}

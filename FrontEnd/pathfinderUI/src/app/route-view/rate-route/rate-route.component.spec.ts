import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateRouteComponent } from './rate-route.component';

describe('RateRouteComponent', () => {
  let component: RateRouteComponent;
  let fixture: ComponentFixture<RateRouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

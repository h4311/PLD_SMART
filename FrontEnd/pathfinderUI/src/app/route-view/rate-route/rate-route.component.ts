import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-rate-route',
  templateUrl: './rate-route.component.html',
  styleUrls: ['./rate-route.component.css']
})
export class RateRouteComponent implements OnInit {
  stars = [
    { color : 'warn'},
    { color : 'warn'},
    { color : 'warn'},
    { color : 'disabled'},
    { color : 'disabled'}
  ];

  comment = '';
  constructor(private dialogRef: MatDialogRef<RateRouteComponent>) { }

  ngOnInit() {
  }

  ok() {
    let nbStars;
    for (let i = 0; i < 5 ; i++ ) {
      if (this.stars[i].color === 'disabled') {
        nbStars = i;
        break;
      }
      nbStars = 5;
    }

    if (this.comment.length < 1) {
      this.comment = 'no comment';
    }
    const review = {stars: nbStars, comment: this.comment};

    this.dialogRef.close(review);
  }

  onStarClick(index) {
    for (let i = 0; i < index + 1 ; i++ ) {
      this.stars[i]['color'] = 'warn';
    }
    for (let i = index + 1; i < 5 ; i++ ) {
      this.stars[i]['color'] = 'disabled';
    }
  }
}

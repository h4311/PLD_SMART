import {Component, OnDestroy} from '@angular/core';
import {RouteService} from '../services/route.service';
import {Router} from '@angular/router';
import {Route} from '../model/route';
import {Coordinates} from '../model/coordinates';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {CancelRouteConfirmationComponent} from './cancel-route-confirmation/cancel-route-confirmation.component';
import {EndRouteConfirmationComponent} from './end-route-confirmation/end-route-confirmation.component';
import {RateRouteComponent} from './rate-route/rate-route.component';
import {AppComponent} from '../app.component';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-route-view',
  templateUrl: './route-view.component.html',
  styleUrls: ['./route-view.component.css']
})
export class RouteViewComponent implements OnDestroy {

  routeIsBeginned = false;
  routeIsSaved = false;
  route: Route = undefined;
  waypointsCoordinates: Coordinates[];
  stars = [
    {color: 'warn'},
    {color: 'warn'},
    {color: 'warn'},
    {color: 'disabled'},
    {color: 'disabled'}
  ];


  constructor(private routeService: RouteService, private router: Router, private dialog: MatDialog,
              private appComponent: AppComponent) {
    if (!(localStorage.getItem('connectedUser'))) {
      this.router.navigateByUrl('/log-in');
      return;
    }
    if (localStorage.getItem('currentRoute') !== null) {
      this.route = JSON.parse(localStorage.getItem('currentRoute'));
      this.routeIsBeginned = true;
      appComponent.setMenuButton();
    } else {
      this.route = JSON.parse(localStorage.getItem('tempRoute'));
      appComponent.setBackButton();
    }
    this.route.waypoints.forEach(function (element) {
      element['iconUrl'] = 'assets/marker-yellow.png';
      element['passed'] = false;
    });
    this.route.origin['iconUrl'] = 'assets/marker-green.png';
    this.route.destination['iconUrl'] = 'assets/marker-red.png';

    this.waypointsCoordinates = this.route.waypoints.map(poi => poi.coordinates);
  }

  public ngOnDestroy() {
    this.routeService.resetTempRoute();
    console.log('destroyed temp route');
  }

  poiSelected = null;
  infoWindow = null;

  onStarClick(index, pos) {
    for (let i = 0; i < index + 1; i++) {
      this.stars[i]['color'] = 'warn';
    }
    for (let i = index + 1; i < 5; i++) {
      this.stars[i]['color'] = 'disabled';
    }
    pos.rating = index + 1;
  }

  sendPOIRating(pos) {
    this.routeService.addPoiReview(pos.id, pos.rating, 'noComment').subscribe(data => {this.infoWindow.close();} );
  }

  clickedMarker(pos, infoWindow) {
    if (this.infoWindow === infoWindow) {
      return;
    }
    if (this.infoWindow !== null) {
      this.infoWindow.close();
    }
    this.infoWindow = infoWindow;
  }

  selectionedMarkerColor = 'assets/marker-blue.png';

  markerToggle(pos) {
    pos.passed = true;
    if (pos.iconUrl === 'assets/marker-yellow.png') {
      pos.iconUrl = this.selectionedMarkerColor;
    }
  }

  buttonSave() {
    this.routeService.saveRoute().subscribe(
      (route) => {
        this.routeIsSaved = true;
      });
  }

  buttonBegin() {
    this.routeService.startRoute().subscribe(
      (route: Route) => {
        this.routeService.storeCurrentRoute(route);
        this.routeIsBeginned = true;
        this.appComponent.setMenuButton();
      });
  }

  buttonFinish() {
    this.openEndRouteConfirmationModal();
  }


  buttonCancel() {
    this.openCancelRouteConfirmationModal();
  }

  openEndRouteConfirmationModal() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(EndRouteConfirmationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === true) {
          this.routeService.endRoute().subscribe(
            (route) => {
              this.openRatingModal();
            });
        }
      }
    );
  }

  openRatingModal() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(RateRouteComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      review => {
        this.routeService.addReview(this.routeService.currentRoute.id, review.stars, review.comment).subscribe(
          (route) => {
            this.routeService.resetCurrentRoute();
            this.router.navigateByUrl('/');
          },
          error => {
            console.log(error);
            this.routeService.resetCurrentRoute();
            this.router.navigateByUrl('/');
          });
      }
    );
  }

  openCancelRouteConfirmationModal() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(CancelRouteConfirmationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data === true) {
          this.routeService.cancelRoute().subscribe(
            (route) => {
              this.routeService.resetCurrentRoute();
              this.router.navigateByUrl('/');
            });
        }
      }
    );
  }
}

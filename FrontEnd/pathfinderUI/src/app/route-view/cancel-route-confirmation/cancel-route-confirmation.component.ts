import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-cancel-route-confirmation',
  templateUrl: './cancel-route-confirmation.component.html',
  styleUrls: ['./cancel-route-confirmation.component.css']
})
export class CancelRouteConfirmationComponent implements OnInit {

  constructor(    private dialogRef: MatDialogRef<CancelRouteConfirmationComponent>) { }

  ngOnInit() {
  }

  cancelRoute() {
    this.dialogRef.close(true);
  }

  goBack() {
    this.dialogRef.close(false);
  }

}

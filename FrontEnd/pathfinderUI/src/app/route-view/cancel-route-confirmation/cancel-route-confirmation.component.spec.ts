import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelRouteConfirmationComponent } from './cancel-route-confirmation.component';

describe('CancelRouteConfirmationComponent', () => {
  let component: CancelRouteConfirmationComponent;
  let fixture: ComponentFixture<CancelRouteConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelRouteConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelRouteConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../model/user';
import {MatSnackBar} from '@angular/material';
import { RouteService } from '../services/route.service';
import { Route } from '../model/route';
import {AppComponent} from "../app.component";
import {RouteViewComponent} from "../route-view/route-view.component";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: [ './log-in.component.css' ]
})
export class LogInComponent implements OnInit {

  name: string;
  password: string;

  error = 'trying';

  constructor(private userService: UserService, private routeService: RouteService, private router: Router,
              public snackBar: MatSnackBar, private appComponent: AppComponent) {
    appComponent.setMenuButton();
  }

  ngOnInit() {
    if (localStorage.getItem('connectedUser')) {
      this.router.navigateByUrl('/');
    }
  }

  onSubmit() {
    this.userService.loginUser(this.name, this.password)
      .subscribe(
        (result: string) => {
          console.log(result);
          console.log('connection success');
          this.userService.getUser().subscribe(
            next => console.log('get user success'),
            error => console.error('get user failed')
          );
          this.routeService.getCurrentRoute().subscribe(
            (route: Route) => {
              console.log('get current!');
              if (!(route === null)) {
                console.log('a route exist !');
                this.routeService.storeCurrentRoute(route);
              }
            },
            error => console.error(error)
          );
          this.router.navigate(['home']);
          this.openSnackBar('You are connected', 1000);
        },
        error => {
          if (this.name === 'admin' && this.password === 'admin') {
            const user = new User('admin', 'admin', 'admin', 'admin');
            this.userService.updateUser(user);
          } else {
            this.error = 'Failed to connect';
            console.error(error);
          }
          this.openSnackBar('We could not connect you. Check your username and your password', 8000);
        });
  }


  openSnackBar(message: string, time: number) {
    this.snackBar.open(message, null, {
      duration: time,
    });
  }
}

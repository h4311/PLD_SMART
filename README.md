## PathFinder est un générateur automatique de trajets touristiques.

### L’application pathfinder a 2 parties, un back-end et un front-end. Les donnees sont stockees sur une base mysql.

### Pour installer et lancer l’application il faut:
1.Installer et configurer une base de données mysql. Ajouter un utilisateur a la base avec le nom `insah4311` et le password `insah4311pwd` 
et le mode d'authentification `caching_sha2_password`. Créer une schéma avec le nom `insa_h4311_db`. Donner les droits pour cette schema a l’utilisateur.

2.Effectuer `npm install` dans le dossier /FrontEnd/pathfinderUI pour installer le front-end.

3.Effectuer `npm start` dans le même dossier pour lancer le front-end. Normalement il sera accessible sur [localhost:4200](localhost:4200).

4.Vous pouvez générer le .jar du back-end en effectuant `mvn package` dans le dossier /BackEnd/pathfinder

5.Sinon le .jar se trouve dans le dossier /BackEnd/pathfinder/target. 
Il faut exécuter la commande `java -jar pathfinder-0.0.1-SNAPSHOT.jar` pour lancer le serveur back-end. Normalement il sera accessible sur [localhost:8080](localhost:8080).

Si tous les points ont été suivi l’application sera disponible sur [localhost:4200](localhost:4200).

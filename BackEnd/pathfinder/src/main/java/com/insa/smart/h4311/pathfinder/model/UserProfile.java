package com.insa.smart.h4311.pathfinder.model;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * A note must be between 0 and 5 included.
     */
    @ElementCollection
    //@Transient
    private Map<UserType, Integer> notes;

    /**
     * Sets all existing types to note 0;
     */
    public UserProfile() {
        notes = new HashMap<>();
        for (UserType type : UserType.values()){
            notes.put(type, 3);
        }
    }

    public UserProfile(Map<UserType, Integer> notes) {
        this.notes = notes;
    }

    public int getNote(UserType type){
        return notes.get(type);
    }

    public boolean setNote(UserType type, int note){
        if (note < 0 || note > 5)
            return false;
        notes.put(type, note);
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<UserType, Integer> getNotes() {
        return notes;
    }

    public void setNotes(Map<UserType, Integer> notes) {
        this.notes = notes;
    }
}

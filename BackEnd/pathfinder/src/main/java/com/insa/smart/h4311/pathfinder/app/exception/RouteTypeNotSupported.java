package com.insa.smart.h4311.pathfinder.app.exception;

public class RouteTypeNotSupported extends Exception {
    public RouteTypeNotSupported() { super(); }
    public RouteTypeNotSupported(String message) { super(message); }
    public RouteTypeNotSupported(String message, Throwable cause) { super(message, cause); }
    public RouteTypeNotSupported(Throwable cause) { super(cause); }
}

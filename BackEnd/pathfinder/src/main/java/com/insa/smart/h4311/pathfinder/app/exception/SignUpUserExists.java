package com.insa.smart.h4311.pathfinder.app.exception;

public class SignUpUserExists extends Exception {
    public SignUpUserExists() { super(); }
    public SignUpUserExists(String message) { super(message); }
    public SignUpUserExists(String message, Throwable cause) { super(message, cause); }
    public SignUpUserExists(Throwable cause) { super(cause); }
}
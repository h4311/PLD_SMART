package com.insa.smart.h4311.pathfinder.model;

public enum UserType {
    sport,
    drink,
    gastronomic,
    culture,
    //family, //deprecated
    //solo, //deprecated
    other
}

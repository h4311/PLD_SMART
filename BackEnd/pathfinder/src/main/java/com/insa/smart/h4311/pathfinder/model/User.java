package com.insa.smart.h4311.pathfinder.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Column(unique = true)
    private String name;

    @NotEmpty
    @Email(message = "*Please provide a valid Email")
    private String email;

    //TODO: uncomment !
    //@NotEmpty(message = "*Please provide your password")
    //@Length(min = 5, message = "*Your password must have at least 5 characters")
    @JsonIgnore
    private String password;

//    @PostLoad
//    private void removePassword() {
//        this.password = null;
//    }

    @Column(name = "role", columnDefinition = "varchar(255) default 'USER'")
    private String role;

    @OneToOne(cascade = CascadeType.ALL)
    private UserProfile userProfile;

    @ManyToMany
    @ElementCollection
    @JsonIgnore
    private List<Route> routes;

    @ManyToMany
    @ElementCollection
    @JsonIgnore
    private List<Route> savedRoutes;

    @OneToOne
    @JsonIgnore
    private Route currentRoute;

    @OneToOne
    @JsonIgnore
    private PointOfInterest currentPointOfInterest;

    public User() {
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public void addRoute(Route route){
        this.routes.add(route);
    }

    public Optional<Route> getCurrentRoute() {
        return Optional.ofNullable(currentRoute);
    }

    public void setCurrentRoute(Route currentRoute) {
        this.currentRoute = currentRoute;
    }

    public List<Route> getSavedRoutes() {
        return savedRoutes;
    }

    public void setSavedRoutes(List<Route> savedRoutes) {
        this.savedRoutes = savedRoutes;
    }

    public void addSavedRoute(Route route){
        this.savedRoutes.add(route);
    }

    public void removeSavedRoute(Route route){this.savedRoutes.remove(route);}

    public PointOfInterest getCurrentPointOfInterest() {
        return currentPointOfInterest;
    }

    public void setCurrentPointOfInterest(PointOfInterest currentPointOfInterest) {
        this.currentPointOfInterest = currentPointOfInterest;
    }
}

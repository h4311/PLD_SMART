package com.insa.smart.h4311.pathfinder.app.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insa.smart.h4311.pathfinder.app.exception.BadRequestParameter;
import com.insa.smart.h4311.pathfinder.app.exception.RouteTypeNotSupported;
import com.insa.smart.h4311.pathfinder.app.repository.PointOfInterestRepository;
import com.insa.smart.h4311.pathfinder.app.repository.ReviewRepository;
import com.insa.smart.h4311.pathfinder.app.repository.RouteRepository;
import com.insa.smart.h4311.pathfinder.app.repository.UserRepository;
import com.insa.smart.h4311.pathfinder.app.services.RouteService;
import com.insa.smart.h4311.pathfinder.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping(path = "/route")
public class RouteController {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;

    @Autowired
    private RouteService routeService;

    @Autowired
    private UserRepository userRepository;

    Logger log = LoggerFactory.getLogger(RouteController.class.getName());

    @Value("${googlemaps.apikey}")
    private String apiKey;

    /**
     * Gets all the routes from the DB
     *
     * @return all the routes from the DB
     */
    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Route> getAllRoutes() {
        // This returns a JSON or XML with the routes
        return routeRepository.findAll();
    }

    @PostMapping(path = "/generateRouteOriginDestination")
    public @ResponseBody
    Route generateRouteOriginDestination(@RequestBody String requestBody, Principal principal) throws IOException, BadRequestParameter, RouteTypeNotSupported {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(requestBody);
        if (!node.has("origin"))
            throw new BadRequestParameter("The request body should have an 'origin' parameter.");
        if (!node.has("destination"))
            throw new BadRequestParameter("The request body should have a 'destination' parameter.");
        if (!node.has("type"))
            throw new BadRequestParameter("The request body should have a 'type' parameter.");
        if (!node.has("originName"))
            throw new BadRequestParameter("The request body should have a 'originName' parameter.");
        if (!node.has("destinationName"))
            throw new BadRequestParameter("The request body should have a 'destinationName' parameter.");
        Coordinates origin = mapper.convertValue(node.get("origin"), Coordinates.class);
        Coordinates destination = mapper.convertValue(node.get("destination"), Coordinates.class);
        String type = mapper.convertValue(node.get("type"), String.class);
        String originName = mapper.convertValue(node.get("originName"), String.class);
        String destinationName = mapper.convertValue(node.get("destinationName"), String.class);
        Route route = new Route(origin, destination);
        route.setType(type);
        route.setOriginName(originName);
        route.setDestinationName(destinationName);
        route.setWaypoints(routeService.computeWaypoints(origin, destination, type));
        long idExistingRoute = routeService.checkIfRouteExists(route);
        if (idExistingRoute == -1) {
            routeRepository.save(route);
            return route;
        } else {
            log.info("route already exists!");
            Route route1 = routeRepository.getOne(idExistingRoute);
            return route1;
        }
        /*Optional<User> userOptional = userRepository.findByName(principal.getName());
        userOptional.ifPresent(user -> route.setCreator(user));*/
        //todo decide if we should save a route even if the user does not complete it
    }

    @PostMapping(path = "/generateRouteOriginRadius")
    public @ResponseBody
    Route generateRouteOriginRadius(@RequestBody String requestBody, Principal principal) throws IOException, RouteTypeNotSupported, BadRequestParameter {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(requestBody);
        if (!node.has("origin"))
            throw new BadRequestParameter("The request body should have an 'origin' parameter.");
        if (!node.has("radius"))
            throw new BadRequestParameter("The request body should have a 'radius' parameter.");
        if (!node.has("type"))
            throw new BadRequestParameter("The request body should have a 'type' parameter.");
        Coordinates origin = mapper.convertValue(node.get("origin"), Coordinates.class);
        Double radius = mapper.convertValue(node.get("radius"), Double.class);
        String type = mapper.convertValue(node.get("type"), String.class);
        Route route = new Route(origin, radius);
        route.setWaypoints(routeService.computeWaypoints(origin, radius, type));
        /*Optional<User> userOptional = userRepository.findByName(principal.getName());
        userOptional.ifPresent(user -> route.setCreator(user));*/
        //todo decide if we should save a route even if the user does not complete it
        long idExistingRoute = routeService.checkIfRouteExists(route);
        if (idExistingRoute == -1) {
            routeRepository.save(route);
            return route;
        } else {
            return routeRepository.getOne(idExistingRoute);
        }
    }

    @PostMapping(path = "/addReview")
    public @ResponseBody
    Route addReview(@RequestBody String requestBody, Principal principal) throws IOException, BadRequestParameter {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(requestBody);
        if (!node.has("id"))
            throw new BadRequestParameter("The request body should have an 'id' parameter.");
        if (!node.has("note"))
            throw new BadRequestParameter("The request body should have a 'note' parameter.");
        if (!node.has("comment"))
            throw new BadRequestParameter("The request body should have a 'comment' parameter.");
        long id = mapper.convertValue(node.get("id"), long.class);
        int note = mapper.convertValue(node.get("note"), int.class);
        String comment = mapper.convertValue(node.get("comment"),String.class);
        Route route = routeRepository.getOne(id);
        //todo check for null
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        Review review = new Review(comment, note);
        userOptional.ifPresent(review::setUser);
        route.addReview(review);
        reviewRepository.save(review);
        routeRepository.save(route);
        return route;
    }

    @GetMapping(path = "/saveRoute")
    public @ResponseBody
    Route saveRoute(Principal principal, @RequestParam long id) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        //todo check for null
        User user = userOptional.get();
        Route route = routeRepository.getOne(id);
        boolean routeExists = false;
        for(Route route1:user.getSavedRoutes()){
            if(route1.getWaypoints().containsAll(route.getWaypoints())){
                routeExists = true;
            }
        }
        if (!routeExists) {
                user.addSavedRoute(route);
                userRepository.save(user);
        }else{
            log.info("trying to save existing route, no action taken");
        }
        return route;
    }

    @GetMapping(path="/deleteSavedRoute")
    public @ResponseBody
    Route deleteSavedRoute(Principal principal, @RequestParam long id) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        //todo check for null
        Route route = routeRepository.getOne(id);
        userOptional.ifPresent(user -> {
            user.removeSavedRoute(route);
            userRepository.save(user);
        });
        return route;
    }

    @GetMapping(path = "/startRoute")
    public @ResponseBody
    Route startRoute(Principal principal, @RequestParam long id) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        //todo check for null
        Route route = routeRepository.getOne(id);
        userOptional.ifPresent(user -> {
            user.setCurrentRoute(route);
            userRepository.save(user);
        });
        return route;

    }

    @GetMapping(path = "/endRoute")
    public @ResponseBody
    Route endRoute(Principal principal) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Optional<Route> routeOptional = user.getCurrentRoute();
            if (routeOptional.isPresent()) {
                Route route = routeOptional.get();
                if (route.getCreator() == null)
                    route.setCreator(user);
                List<Route> userRoutes = user.getRoutes();
                for(Route route1:userRoutes){
                    if(route1.getId()==route.getId()) {
                        route1.setDateTime(LocalDateTime.now());
                        routeRepository.save(route1);
                        return route1;
                    }
                }
                LocalDateTime now = LocalDateTime.now();
                route.setDateTime(now);
                routeRepository.save(route);
                user.addRoute(route);
                user.setCurrentRoute(null);
                userRepository.save(user);
                //todo decide if we should save the route here or at its creation
                return route;
            } else {
                return null;
                //todo throw exception rather than return null, or think about something else
            }
        } else {
            return null;
        }
    }

    @GetMapping(path = "/getCurrentRoute")
    public @ResponseBody
    Route getCurrentRoute(Principal principal) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        return userOptional.flatMap(User::getCurrentRoute).orElse(null);
    }

    @GetMapping(path = "/cancelRoute")
    public @ResponseBody
    ResponseEntity cancelRoute(Principal principal) {
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        Optional<Route> routeOptional = user.getCurrentRoute();
        if (routeOptional.isPresent()) {
            Route route = routeOptional.get();
            user.setCurrentRoute(null);
            userRepository.save(user);
            if(route.getCreator()==null)
                routeRepository.delete(route);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping(path="/getRecommended")
    public @ResponseBody List<Route> getRecommended(Principal principal){
        List<Route> recommendedRoutes = new ArrayList<>();
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        List<Map.Entry<UserType, Integer>> entries = new ArrayList<>(
                user.getUserProfile().getNotes().entrySet()
        );
        entries.sort((a, b) -> Integer.compare(b.getValue(), a.getValue()));
        String firstType = entries.get(0).getKey().name();
        String secondType = entries.get(1).getKey().name();
        List<Route> routeListFirstType = routeRepository.getAllByTypeOrderByRatingDesc(firstType);
        List<Route> routeListSecondType = routeRepository.getAllByTypeOrderByRatingDesc(secondType);
        for(Route route:routeListFirstType){
            if((route.getCreator()!=null)&&(route.getCreator().getName()!=user.getName())){
                //log.info(route.getCreator().getName()+"    "+user.getName());
                recommendedRoutes.add(route);
                if(recommendedRoutes.size()==2){
                    break;
                }
            }
        }
        int countSecondType = 0;
        for(Route route:routeListSecondType){
            if((route.getCreator()!=null)&&(route.getCreator().getName()!=user.getName())){
                recommendedRoutes.add(route);
                countSecondType++;
                if(countSecondType==2){
                    break;
                }
            }
        }
        return recommendedRoutes;
    }

    @GetMapping(path="/getTop")
    public @ResponseBody List<Route> getTop(){
        List<Route> topRoutes = new ArrayList<>();
        for(UserType userType:UserType.values()){
            List<Route> routesByType = routeRepository.getAllByTypeOrderByRatingDesc(userType.name());
            if ((routesByType.size() > 0)&&(routesByType.size()<10)) {
                topRoutes.addAll(routesByType);
            }
            if (routesByType.size() > 10) {
                topRoutes.addAll(routesByType.subList(0,9));
            }
        }
        return topRoutes;
    }

//    @GetMapping(path="/getTopByType")
//    public @ResponseBody List<Route> getTopByType(@RequestParam String type){
//        List<Route> topRoutes = new ArrayList<>();
//        List<Route> routesByType = routeRepository.getAllByTypeOrderByRatingDesc(type);
//        for(Route route:routesByType){
//            if((route.getCreator()==null)){
//                routesByType.remove(route);
//            }
//        }
//        if ((routesByType.size() > 0)&&(routesByType.size()<10)) {
//            topRoutes.addAll(routesByType);
//        }
//        if (routesByType.size() > 10) {
//            topRoutes.addAll(routesByType.subList(0,9));
//        }
//        return topRoutes;
//    }

    @GetMapping(path="/getAllByType")
    public @ResponseBody List<Route> getAllSorted(@RequestParam String type){
        List<Route> routesSorted = new ArrayList<>();
        List<Route> routesByType = routeRepository.getAllByTypeOrderByRatingDesc(type);
        for(Route route:routesByType){
            if((route.getCreator()!=null)){
                routesSorted.add(route);
            }
        }
        return routesSorted;
    }

    @GetMapping(path="/getHistory")
    public @ResponseBody List<Route> getHistory(Principal principal){
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        return user.getRoutes();
    }

    @GetMapping(path="/getSaved")
    public @ResponseBody List<Route> getSaved(Principal principal){
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        return user.getSavedRoutes();
    }

    @GetMapping(path="/passPoint")
    public @ResponseBody PointOfInterest passPoint(@RequestParam long pointOfInterestId, Principal principal){
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        PointOfInterest pointOfInterest = pointOfInterestRepository.getOne(pointOfInterestId);
        if ((user.getCurrentRoute() != null) && (user.getCurrentRoute().get().getWaypoints().contains(pointOfInterest))) {
            user.setCurrentPointOfInterest(pointOfInterest);
        }
        userRepository.save(user);
        return pointOfInterest;
    }

    @GetMapping(path="/getPassedPoint")
    public @ResponseBody PointOfInterest getPassedPoint(Principal principal){
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        User user = userOptional.get();
        if(user.getCurrentRoute()!=null) {
            PointOfInterest pointOfInterest = user.getCurrentPointOfInterest();
            return pointOfInterest;
        }
        return null;
    }
}

package com.insa.smart.h4311.pathfinder.app.task;

import com.google.maps.*;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.PlaceDetails;
import com.insa.smart.h4311.pathfinder.app.repository.PointOfInterestRepository;
import com.insa.smart.h4311.pathfinder.model.Coordinates;
import com.insa.smart.h4311.pathfinder.model.UserType;
import com.insa.smart.h4311.pathfinder.model.PointOfInterest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class UpdatePOITask {

    private static final Logger log = LoggerFactory.getLogger(UpdatePOITask.class);

    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;

    @Value("${googlemaps.apikey}")
    private String apiKey;

    //update the POI list every 2 hours
    // TODO uncomment in production
    //@Scheduled(fixedRate = 7200000)
    public void updatePOI() {
        log.info("updating POIs");
        List<PointOfInterest> finalPOIs = new ArrayList<>();
        try {
            finalPOIs.addAll(updateGrandLyonPOI());
            updateGoogleMapsPOI(finalPOIs);
            pointOfInterestRepository.saveAll(finalPOIs);
            log.info("Finished updating POIs.");
        } catch (IOException e) {
            log.error("Error when updating points of interest", e);
        }
    }

    private List<PointOfInterest> updateGrandLyonPOI() throws IOException {
        List<PointOfInterest> grandLyonPointOfInterests = new ArrayList<>();
        // retrieve data from grand lyon API url
        URL grandLyonURL = new URL("https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&typename=sit_sitra.sittourisme&SRSNAME=urn:ogc:def:crs:EPSG::4326");
        URLConnection grandLyonURLConnection = grandLyonURL.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(grandLyonURLConnection.getInputStream()));
        StringBuilder sb = new StringBuilder();
        in.lines().forEach(s -> sb.append(s).append(System.getProperty("line.separator")));
        // parse the JSON obtained as response
        JSONArray values = new JSONObject(sb.toString()).getJSONArray("features");
        for (int i = 0; i < values.length(); ++i) {
            JSONObject value = values.getJSONObject(i);
            JSONObject properties = value.getJSONObject("properties");
            String grandLyonId = properties.getString("id");
            String name = properties.getString("nom");
            //if (name.isEmpty()) log.info("Empty name for id " + grandLyonId);
            String address = properties.getString("adresse");
            //if (address.isEmpty()) log.info("Empty address for id " + grandLyonId);
            address += ", " + properties.getString("codepostal") + " Lyon, France";
            String mainType = properties.getString("type");
            String typesString = properties.getString("type_detail");
            List<String> types = new ArrayList<>();
            if (!typesString.isEmpty()) {
                types.addAll(Arrays.asList(typesString.split(";")));
            }
            JSONArray coordinatesJSON = value.getJSONObject("geometry").getJSONArray("coordinates");
            Coordinates coordinates = new Coordinates(coordinatesJSON.getDouble(1), coordinatesJSON.getDouble(0));
            PointOfInterest poi = new PointOfInterest(grandLyonId, name, address, mainType, types, coordinates);
            // check if the POI exits in our database already
            // if yes, then we need to set it's id in order to force an update and not an insert
            Optional<PointOfInterest> existingPointOfInterest = pointOfInterestRepository.findByGrandLyonId(grandLyonId);
            existingPointOfInterest.ifPresent(pointOfInterest -> {
                poi.setId(pointOfInterest.getId());
                poi.setGoogleMapsId(pointOfInterest.getGoogleMapsId());
            });
            grandLyonPointOfInterests.add(poi);
        }
        return grandLyonPointOfInterests;
    }

    private void updateGoogleMapsPOI(List<PointOfInterest> pointOfInterests) {
        for (PointOfInterest pointOfInterest : pointOfInterests) {
            if (pointOfInterest.getId() != null && pointOfInterest.getGoogleMapsId() != null) {
                updateGooglePlacesPOI(pointOfInterest);
            } else if (pointOfInterest.getId() == null) {
                GeoApiContext geoApiContext = new GeoApiContext.Builder().apiKey(apiKey).build();
                GeocodingApiRequest geocodingApiRequest = GeocodingApi.newRequest(geoApiContext);
                String originalAddress = pointOfInterest.getAddress();
                String addressToSearch = originalAddress.charAt(0) == ',' ?
                        pointOfInterest.getName() + originalAddress : originalAddress;
                geocodingApiRequest.address(addressToSearch);
                geocodingApiRequest.language("fr");
                PendingResult.Callback<GeocodingResult[]> callback = new PendingResult.Callback<GeocodingResult[]>() {
                    @Override
                    public void onResult(GeocodingResult[] geocodingResult) {
                        if (geocodingResult.length > 0) {
                            // set google maps id
                            pointOfInterest.setGoogleMapsId(geocodingResult[0].placeId);
                            updateGooglePlacesPOI(pointOfInterest);
                        } else {
                            log.info("No result from geocoding API with address '" + addressToSearch + "'");
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        log.error("Error on request to geocoding API with address '" + addressToSearch + "'", throwable);
                    }
                };
                geocodingApiRequest.setCallback(callback);
            }
        }
    }

    private void updateGooglePlacesPOI(PointOfInterest pointOfInterest) {
        GeoApiContext geoApiContext = new GeoApiContext.Builder().apiKey(apiKey).build();
        PlaceDetailsRequest placeDetailsRequest = PlacesApi.placeDetails(geoApiContext, pointOfInterest.getGoogleMapsId());
        PendingResult.Callback<PlaceDetails> callback = new PendingResult.Callback<PlaceDetails>() {
            @Override
            public void onResult(PlaceDetails placeDetails) {
                pointOfInterest.setRating(placeDetails.rating);
                // set full address if the data from grand lyon didn't provide it
                if (pointOfInterest.getAddress().charAt(0) == ',')
                    pointOfInterest.setAddress(placeDetails.formattedAddress);
                // set coordinates from google for better accuracy
                pointOfInterest.setCoordinates(new Coordinates(placeDetails.geometry.location));
                if (placeDetails.types != null) pointOfInterest.setTypes(Arrays.asList(placeDetails.types));
            }

            @Override
            public void onFailure(Throwable throwable) {
                log.error("Error on request to places API with placeId '" + pointOfInterest.getGoogleMapsId() + "'", throwable);
            }
        };
        placeDetailsRequest.setCallback(callback);
    }
}
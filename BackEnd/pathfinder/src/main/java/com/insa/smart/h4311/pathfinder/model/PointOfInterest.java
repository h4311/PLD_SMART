package com.insa.smart.h4311.pathfinder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PointOfInterest implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String grandLyonId;

    private String googleMapsId;

    private String name;

    @Embedded
    private Coordinates coordinates;

    private String address;

    private Float rating;

    private String mainType;

    private double userRating;

    private int reviewsNr;

    @OneToMany
    private List<Review> reviews;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> types;

    public PointOfInterest() {
    }

    public PointOfInterest(String grandLyonId, String name, String address, String mainType, List<String> types, Coordinates coordinates) {
        this.grandLyonId = grandLyonId;
        this.name = name;
        this.address = address;
        this.mainType = mainType;
        this.types = types;
        this.coordinates = coordinates;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGrandLyonId() {
        return grandLyonId;
    }

    public void setGrandLyonId(String grandLyonId) {
        this.grandLyonId = grandLyonId;
    }

    public String getGoogleMapsId() {
        return googleMapsId;
    }

    public void setGoogleMapsId(String googleMapsId) {
        this.googleMapsId = googleMapsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public List<String> getTypes() {
        return types;
    }

    /**
     * Sets types from a POITypes list.
     */
    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getMainType() {
        return mainType;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setMainType(String mainType) {
        this.mainType = mainType;
    }

    public double getUserRating() {
        return userRating;
    }

    public void setUserRating(double userRating) {
        this.userRating = userRating;
    }

    public int getReviewsNr() {
        return reviewsNr;
    }

    public void setReviewsNr(int reviewsNr) {
        this.reviewsNr = reviewsNr;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
        this.reviewsNr=reviews.size();
        this.userRating=computeUserRating();
    }

    public void addReview(Review review){
        this.reviews.add(review);
        this.reviewsNr++;
        this.userRating=computeUserRating();
    }

    private double computeUserRating(){
        int sum=0;
        for(Review review:reviews){
            sum+=review.getNote();
        }
        if(reviews.size()!=0)
            return (double)sum/reviews.size();
        else return 0;
    }
}

//package com.insa.smart.h4311.pathfinder.app.config;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//
//@Configuration
//public class DataSourceConfig {
//    @Bean
//    @ConfigurationProperties("spring.datasource")
//    public DataSource dataSource() {
//        return DataSourceBuilder.create().username("sa")
//                .password("")
//                .url("jdbc:h2:mem:testdb;DB_CLOSE_ON_EXIT=FALSE")
//                .driverClassName("org.h2.Driver")
//                .build();
//    }
//}

package com.insa.smart.h4311.pathfinder.app.controller;

import com.insa.smart.h4311.pathfinder.app.exception.SignUpUserExists;
import com.insa.smart.h4311.pathfinder.model.UserProfile;
import com.insa.smart.h4311.pathfinder.model.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.insa.smart.h4311.pathfinder.model.User;
import com.insa.smart.h4311.pathfinder.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    private static final Logger log = LoggerFactory.getLogger(UserController.class.getName());

    @PostMapping(path = "/add") // Map ONLY GET Requests
    public @ResponseBody
    User addNewUser(@RequestParam String name, @RequestParam String email, @RequestParam String password) throws SignUpUserExists {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request
        Optional<User> optionalUser = userRepository.findByName(name);
        if(optionalUser.isPresent()){
            throw new SignUpUserExists("an user with this name exists");
        }
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setRole("USER");
        UserProfile userProfile = new UserProfile();
        user.setUserProfile(userProfile);
        userRepository.save(user);
        return user;
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        // This returns a JSON or XML with the users
        return userRepository.findAll();
    }

    @GetMapping(path = "/getLoggedUser")
    public @ResponseBody
    User getLoggedUser(Principal principal) {
        Optional<User> optionalUser = userRepository.findByName(principal.getName());
        return optionalUser.orElse(null);
    }

    @PostMapping(path = "/configurePreferences")
    public @ResponseBody
    User configurePreferences(Principal userPrincipal,
                              @RequestParam("sport") int sport,
                              //@RequestParam("family") int family,
                              @RequestParam("drink") int drink,
                              @RequestParam("gastronomic") int gastronomic,
                              @RequestParam("culture") int culture) {
        //@RequestParam("solo") int solo){
        User user;
        Optional<User> optionalUser = userRepository.findByName(userPrincipal.getName());
        user = optionalUser.get();
        //UserProfile oldUserProfile = user.getUserProfile();
        UserProfile userProfile = new UserProfile();
        userProfile.setNote(UserType.sport, sport);
        //userProfile.setNote(UserType.family,family);
        userProfile.setNote(UserType.drink, drink);
        userProfile.setNote(UserType.gastronomic, gastronomic);
        userProfile.setNote(UserType.culture, culture);
        //userProfile.setNote(UserType.solo,solo);
        user.setUserProfile(userProfile);
        userRepository.save(user);
        return user;
    }

}

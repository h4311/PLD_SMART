package com.insa.smart.h4311.pathfinder.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private User creator;

    @OneToMany
    private List<Review> reviews;

    private double rating;

    private LocalDateTime dateTime;

    private int reviewsNr;

    private String originName;

    private String destinationName;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "latitude", column = @Column(name = "origin_lat")),
            @AttributeOverride(name = "longitude", column = @Column(name = "origin_lng")),
            @AttributeOverride(name = "distance", column = @Column(name = "origin_distance")),
    })
    private Coordinates origin;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "latitude", column = @Column(name = "destination_lat")),
            @AttributeOverride(name = "longitude", column = @Column(name = "destination_lng")),
            @AttributeOverride(name = "distance", column = @Column(name = "destination_distance")),
    })
    private Coordinates destination;

    @ManyToMany
    private List<PointOfInterest> waypoints;

    @Column(nullable = true)
    private Double radius;

    private String type;

    public Route(Coordinates origin, Coordinates destination, List<PointOfInterest> waypoints, User creator) {
        this.origin = origin;
        this.destination = destination;
        this.waypoints = waypoints;
        this.creator = creator;
    }

    public Route() {
    }

    public Route(Coordinates origin, double radius) {
        this.origin = origin;
        this.radius = radius;
    }

    public Route(Coordinates origin, Coordinates destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public Route(Coordinates origin, Coordinates destination, String type) {
        this.origin = origin;
        this.destination = destination;
        this.type = type;
    }


    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
        this.reviewsNr = reviews.size();
        this.rating = computeRating();
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getReviewsNr() {
        return reviewsNr;
    }

    public void setReviewsNr(int reviewsNr) {
        this.reviewsNr = reviewsNr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Coordinates getOrigin() {
        return origin;
    }

    public void setOrigin(Coordinates origin) {
        this.origin = origin;
    }

    public Coordinates getDestination() {
        return destination;
    }

    public void setDestination(Coordinates destination) {
        this.destination = destination;
    }

    public List<PointOfInterest> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<PointOfInterest> waypoints) {
        this.waypoints = waypoints;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    private double computeRating() {
        int sum = 0;
        for (Review review : reviews) {
            sum += review.getNote();
        }
        if (reviews.size() != 0)
            return (double) sum / reviews.size();
        else return 0;
    }

    public void addReview(Review review){
        this.reviews.add(review);
        this.reviewsNr++;
        this.rating=computeRating();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }
}

package com.insa.smart.h4311.pathfinder.model;

import com.google.maps.model.LatLng;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Coordinates implements Serializable {
    private Double latitude;

    private Double longitude;

    public Coordinates() {}

    public Coordinates(LatLng latLng) {
        this.latitude = latLng.lat;
        this.longitude = latLng.lng;
    }

    public Coordinates(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public LatLng toLatLng(){
        return new LatLng(latitude, longitude);
    }
}

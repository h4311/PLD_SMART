package com.insa.smart.h4311.pathfinder.app.repository;

import com.insa.smart.h4311.pathfinder.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review,Long> {
}

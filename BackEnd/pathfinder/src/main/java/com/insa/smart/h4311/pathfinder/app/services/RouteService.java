package com.insa.smart.h4311.pathfinder.app.services;

import com.insa.smart.h4311.pathfinder.app.exception.RouteTypeNotSupported;
import com.insa.smart.h4311.pathfinder.app.repository.PointOfInterestRepository;
import com.insa.smart.h4311.pathfinder.app.repository.RouteRepository;
import com.insa.smart.h4311.pathfinder.model.Coordinates;
import com.insa.smart.h4311.pathfinder.model.PointOfInterest;
import com.insa.smart.h4311.pathfinder.model.ProfileTypeMapping;
import com.insa.smart.h4311.pathfinder.model.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RouteService {

    Logger log = LoggerFactory.getLogger(RouteService.class.getName());

    @Value("${googlemaps.apikey}")
    private String apiKey;

    @Autowired
    PointOfInterestRepository pointOfInterestRepository;

    @Autowired
    RouteRepository routeRepository;

    public List<PointOfInterest> computeWaypoints(Coordinates origin, Coordinates destination, String type) throws RouteTypeNotSupported {
        double distance = calculateDistance(origin, destination);
        Coordinates midpoint = calculateMidpoint(origin, destination);
        if (!ProfileTypeMapping.profileTypeMap.containsKey(type))
            throw new RouteTypeNotSupported("The route type '" + type + "' is not supported");
        List<String> types = ProfileTypeMapping.profileTypeMap.get(type);
        List<PointOfInterest> pointOfInterests = pointOfInterestRepository.findPOIWithinRadius(distance / 2,
                midpoint.getLatitude(), midpoint.getLongitude());
        List<PointOfInterest> finalPOIs = new ArrayList<>();
        for(PointOfInterest pointOfInterest : pointOfInterests) {
            List<String> typesTemp = new ArrayList<>(types);
            typesTemp.retainAll(pointOfInterest.getTypes());
            if (!typesTemp.isEmpty()) {
                boolean wellSpaced = true;
                for (PointOfInterest poi : finalPOIs) {
                    double distanceWithChosen = calculateDistance(pointOfInterest.getCoordinates(), poi.getCoordinates());
                    if (distanceWithChosen < distance / 10) {
                        wellSpaced = false;
                        break;
                    }
                }
                if (wellSpaced) {
                    finalPOIs.add(pointOfInterest);
                    if (finalPOIs.size() == 5) break;
                }
            }
        }
        return finalPOIs;
    }

    public List<PointOfInterest> computeWaypoints(Coordinates origin, double radius, String type) throws RouteTypeNotSupported {
        if (!ProfileTypeMapping.profileTypeMap.containsKey(type))
            throw new RouteTypeNotSupported("The route type '" + type + "' is not supported");
        List<String> types = ProfileTypeMapping.profileTypeMap.get(type);
        List<PointOfInterest> pointOfInterests = pointOfInterestRepository.findPOIWithinRadius(radius,
                origin.getLatitude(), origin.getLongitude());
        return pointOfInterests.stream()
                .filter(pointOfInterest -> {
                    List<String> typesTemp = new ArrayList<>(types);
                    typesTemp.retainAll(pointOfInterest.getTypes());
                    return !typesTemp.isEmpty();
                })
                .limit(5)
                .collect(Collectors.toList());
    }

    public long checkIfRouteExists(Route route){
        long id=-1;
        List<Route> routes = routeRepository.getAllByOriginIsAndDestinationIs(route.getOrigin(),route.getDestination());
        for(Route route1:routes){
            if(route1.getWaypoints().containsAll(route.getWaypoints())){
                id=route1.getId();
            }
        }
        return id;
    }

    private double calculateDistance(Coordinates p1, Coordinates p2){
        double R = 6371000; // Earth’s mean radius in m
        double lat1Rad = Math.toRadians(p1.getLatitude());
        double lng1Rad = Math.toRadians(p1.getLongitude());
        double lat2Rad = Math.toRadians(p2.getLatitude());
        double lng2Rad = Math.toRadians(p2.getLongitude());
        double a = Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.cos(lng2Rad - lng1Rad) +
                        Math.sin(lat1Rad) * Math.sin(lat2Rad);
        return R * Math.acos(a);
    }

    private Coordinates calculateMidpoint(Coordinates p1, Coordinates p2) {
        double lat1Rad = Math.toRadians(p1.getLatitude());
        double lng1Rad = Math.toRadians(p1.getLongitude());
        double lat2Rad = Math.toRadians(p2.getLatitude());
        double lng2Rad = Math.toRadians(p2.getLongitude());
        double Bx = Math.cos(lat2Rad) * Math.cos(lng2Rad-lng1Rad);
        double By = Math.cos(lat2Rad) * Math.sin(lng2Rad-lng1Rad);
        double latMid = Math.atan2(Math.sin(lat1Rad) + Math.sin(lat2Rad),
        Math.sqrt((Math.cos(lat1Rad) + Bx) * (Math.cos(lat1Rad) + Bx) + By * By ));
        double lngMid = lng1Rad + Math.atan2(By, Math.cos(lat1Rad) + Bx);
        return new Coordinates(Math.toDegrees(latMid), Math.toDegrees(lngMid));
    }
}

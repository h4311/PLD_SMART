package com.insa.smart.h4311.pathfinder.app.repository;

import com.insa.smart.h4311.pathfinder.model.PointOfInterest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PointOfInterestRepository extends JpaRepository<PointOfInterest,Long> {

    Optional<PointOfInterest> findByGrandLyonId(String grandLyonId);

    Optional<PointOfInterest> findByGoogleMapsId(String googleMapsId);

    @Query(value =
            "SELECT *," +
            "(6371000 * acos(cos(radians(:lat)) * cos(radians(p.latitude)) * cos(radians(p.longitude) - radians(:lng)) + " +
            "sin(radians(:lat)) * sin(radians(p.latitude)))) distance " +
            "FROM point_of_interest p " +
            "HAVING distance < :radius " +
            "ORDER BY rating DESC ",
            nativeQuery = true)
    List<PointOfInterest> findPOIWithinRadius(@Param("radius") double radius, @Param("lat") double latitude, @Param("lng") double longitude);
}

package com.insa.smart.h4311.pathfinder.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class LoginController {

    Logger log = LoggerFactory.getLogger(LoginController.class.getName());

    @GetMapping(value = "/")
    public ResponseEntity getMainPage() {
        log.info("Authorised main page access");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping("/info")
    public @ResponseBody Principal userInfo(Principal principal) {
        return principal;
    }

    @RequestMapping("/userinfo")
    public @ResponseBody String user(Principal user) {
        return user.getName();
    }


//    @PreAuthorize("hasRole('USER')")
//    @PostMapping("/login")
//    public ResponseEntity login() {
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}

package com.insa.smart.h4311.pathfinder.app.exception;

public class BadRequestParameter extends Exception {
    public BadRequestParameter() { super(); }
    public BadRequestParameter(String message) { super(message); }
    public BadRequestParameter(String message, Throwable cause) { super(message, cause); }
    public BadRequestParameter(Throwable cause) { super(cause); }
}
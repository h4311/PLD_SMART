package com.insa.smart.h4311.pathfinder.app.controller;

import com.insa.smart.h4311.pathfinder.app.exception.BadRequestParameter;
import com.insa.smart.h4311.pathfinder.app.exception.RouteTypeNotSupported;
import com.insa.smart.h4311.pathfinder.app.exception.SignUpUserExists;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler({BadRequestParameter.class,SignUpUserExists.class})
    void handleBadRequests(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }


    @ExceptionHandler(RouteTypeNotSupported.class)
    void handleNotImplementedRequests(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_IMPLEMENTED.value());
    }
}

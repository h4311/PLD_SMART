package com.insa.smart.h4311.pathfinder.app.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insa.smart.h4311.pathfinder.app.exception.BadRequestParameter;
import com.insa.smart.h4311.pathfinder.app.repository.PointOfInterestRepository;
import com.insa.smart.h4311.pathfinder.app.repository.ReviewRepository;
import com.insa.smart.h4311.pathfinder.app.repository.UserRepository;
import com.insa.smart.h4311.pathfinder.model.PointOfInterest;
import com.insa.smart.h4311.pathfinder.model.Review;
import com.insa.smart.h4311.pathfinder.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping(path="/pointofinterest")
public class PointOfInterestController {
    @Autowired
    private PointOfInterestRepository pointOfInterestRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(PointOfInterestController.class.getName());

    @GetMapping(path="/all")
    public @ResponseBody Iterable<PointOfInterest> getAllPointsOfInterest() {
        // This returns a JSON or XML with the points of interest
        log.info("returning all of the POIs");
        return pointOfInterestRepository.findAll();
    }

    @PostMapping(path="/addReview")
    public @ResponseBody PointOfInterest addReview(@RequestBody String requestBody, Principal principal) throws BadRequestParameter, IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(requestBody);
        if (!node.has("id"))
            throw new BadRequestParameter("The request body should have an 'id' parameter.");
        if (!node.has("note"))
            throw new BadRequestParameter("The request body should have a 'note' parameter.");
        if (!node.has("comment"))
            throw new BadRequestParameter("The request body should have a 'comment' parameter.");
        long id = mapper.convertValue(node.get("id"), long.class);
        int note = mapper.convertValue(node.get("note"), int.class);
        String comment = mapper.convertValue(node.get("comment"),String.class);
        PointOfInterest pointOfInterest = pointOfInterestRepository.getOne(id);
        Optional<User> userOptional = userRepository.findByName(principal.getName());
        Review review = new Review(comment,note);
        userOptional.ifPresent(user -> review.setUser(user));
        pointOfInterest.addReview(review);
        reviewRepository.save(review);
        pointOfInterestRepository.save(pointOfInterest);
        return pointOfInterest;
    }
}

package com.insa.smart.h4311.pathfinder.app.repository;

import com.insa.smart.h4311.pathfinder.model.Coordinates;
import com.insa.smart.h4311.pathfinder.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RouteRepository extends JpaRepository<Route,Long> {

    List<Route> getAllByOriginIsAndDestinationIs(Coordinates origin,Coordinates destination);
    List<Route> getAllByTypeOrderByRatingDesc(String type);
}
